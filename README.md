# MS-DOS RS232 ESP8266 Internet Networking

A project to enable internet on ms-dos-compatible computers using an ESP8266 connected via an RS232 port. Consists of the hardware firmware, an ms-dos driver and an example node.js server. Goal is to use only secure sockets.